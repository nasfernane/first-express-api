const express = require('express');
const path = require('path');
const friendsRouter = require('./routes/friends.router');
const messagesRouter = require('./routes/messages.router');

const PORT = 3000;

const app = express();

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));


// rend le dossier publique
app.use('/site', express.static(path.join(__dirname, 'public')));
// auto parse le body en json
app.use(express.json()); 

// log la requête avec le temps de réponse de la requête
app.use((req, res, next) => {
  const start = Date.now();
  next();
  const delta = Date.now() - start;
  console.log(`${req.method}: ${req.baseUrl}${req.url} - ${delta}ms`);
})


app.get('/', (req, res) => {
  res.render('index', {
    title: "Where is furimi ?",
    caption: "A mystery to solve"
  })
})

app.use('/friends', friendsRouter);
app.use('/messages', messagesRouter)

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
})