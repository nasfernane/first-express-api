const friends = [
  {
    id: 0,
    name: 'Jean-René'
  },
  {
    id: 1,
    name: 'Jean-Marcel'
  },
  {
    id: 2,
    name: 'Jean-Pascal'
  },
];

function getFriends() {
  return friends;
}

function getFriendById(id) {
  return id < friends.length ? friends[id] : null;
}

function postFriend(name) {
  const newFriend = {
    id: friends.length,
    name
  }

  friends.push(newFriend);

  return newFriend;
}

module.exports = {
  getFriends,
  getFriendById,
  postFriend,
}