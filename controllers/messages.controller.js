const path = require('path');

function getMessages(req, res){
  // const filePath = path.join(__dirname, '..', 'public', 'images', 'photo-ski.jpg');
  // res.sendFile(filePath);
  // res.send('<ul><li>Hello roberto</li></ul>')
  res.render('messages',  {
    time: 3,
    friend: "Alberto"
  })
}

function postMessage(req, res) {
  res.send('Updating message')
}

module.exports = {
  getMessages,
  postMessage,
}