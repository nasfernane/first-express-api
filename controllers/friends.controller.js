const friendsModel = require('../models/friends.model');

function getFriends(req, res) {
  const friends = friendsModel.getFriends();

  if (friends) {
    res.json(friends);
  } else {
    res.status(404).json({
      error: "You have no friends"
    })
  }
}

function getFriendById(req, res)  {
  const friendId = Number(req.params.id);
  const friend = friendsModel.getFriendById(friendId);

  if (friend) {
    res.status(200).json(friend);
  } else {
    res.status(404).json({
      error: "Friend does not exist"
    })
  }
}

function postFriend(req, res) {
  if (!req.body.name) {
    return res.status(400).json({
      clientError: "Missing friend name"
    });
  }

  const friendName = req.body.name;
  const newFriend = friendsModel.postFriend(friendName);

  res.json(newFriend);
}

module.exports = {
  getFriends,
  getFriendById,
  postFriend,
}